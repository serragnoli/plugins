package com.sky.app

import java.io.File
import java.net.URLClassLoader

import com.sky.plugin.contract.Plugin

object Main extends App {
  val pluginPath = sys.props("dbPluginPath")

  val dbPluginJar = new File(pluginPath)
  val authorizedLoader = URLClassLoader.newInstance(Array(dbPluginJar.toURI.toURL))
  val authorizedPlugin: Plugin = authorizedLoader.loadClass(sys.props("entryPoint")).newInstance.asInstanceOf[Plugin]

  authorizedPlugin.process("Hello World")
}
