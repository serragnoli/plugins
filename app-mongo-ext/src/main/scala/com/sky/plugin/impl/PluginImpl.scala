package com.sky.plugin.impl

import java.util

import com.mongodb.MongoClient
import com.sky.plugin.contract.Plugin
import org.bson.Document

class PluginImpl extends Plugin {

  override def process(value: String): Unit = {
    //    Policy.setPolicy(new Nothing)
    //    System.setSecurityManager(new SecurityManager)

    val mongoClient = new MongoClient("127.0.0.1", 27017)
    val database = mongoClient.getDatabase("test")
    val coll = database.getCollection("fabsCollection")

    val document = new Document("name", value)
      .append("contact", new Document("phone", "228-555-0149")
        .append("email", "cafeconleche@example.com")
        .append("location", util.Arrays.asList(-73.92502, 40.8279556)))
      .append("stars", 3)
      .append("categories", util.Arrays.asList("Bakery", "Coffee", "Pastries"))

    coll.insertOne(document)
  }
}
