**Docker DB**

`docker run -d -p 27017:27017 -p 28017:28017 -e AUTH=no tutum/mongodb`

and

`docker run -d --name couchbase -p 8091-8094:8091-8094 -p 11210:11210 couchbase`

**Building**

`mvn clean install` from root of the project

**Usage**

Use Couchbase

`java -jar -DdbPluginPath=<PATH_TO_CB>/app-couchbase-ext-1.0-SNAPSHOT.jar -DentryPoint=com.sky.plugin.impl.PluginImpl app-main/target/app-main-1.0-SNAPSHOT.jar`

Use Mongo

`java -jar -DdbPluginPath=<PATH_TO_MONGO>/app-mongo-ext-1.0-SNAPSHOT.jar -DentryPoint=com.sky.plugin.impl.PluginImpl app-main/target/app-main-1.0-SNAPSHOT.jar`