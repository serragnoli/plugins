package com.sky.plugin.impl

import com.couchbase.client.java.CouchbaseCluster
import com.couchbase.client.java.document.JsonDocument
import com.couchbase.client.java.document.json.JsonObject
import com.sky.plugin.contract.Plugin

class PluginImpl extends Plugin {

  override def process(value: String): Unit = {
//    Policy.setPolicy(new Nothing)
//    System.setSecurityManager(new SecurityManager)


    // Create a cluster reference// Create a cluster reference
    val cluster = CouchbaseCluster.create("127.0.0.1")

    // Connect to the bucket and open it
    val bucket = cluster.openBucket("default")

    // Create a JSON document and store it with the ID "helloworld"
    val content = JsonObject.create.put("hello", value)
    val inserted = bucket.upsert(JsonDocument.create("helloworld", content))

    // Read the document and print the "hello" field
    val found = bucket.get("helloworld")
    System.out.println("Couchbase is the best database in the " + found.content.getString("hello"))

    // Close all buckets and disconnect
    cluster.disconnect
  }
}
